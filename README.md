#music_player
Flutter app to show list of Music available using iTunes Api.

##Supported devices
Supported on mobiles with Android 4.1 and above.
Havent tested it on iOS yet.

##Supported features

Search Music based or artist,album or song name.
Music Player to play the preview of the music along with seek,play,pause as well as skip to next/previous track.



##Requirements and Instructions to build and deploy the app

flutter clean
flutter pub get
flutter build apk

and it will run the app on one of the android simulators