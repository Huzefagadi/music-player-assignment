import 'package:get_it/get_it.dart';
import 'package:music_player/core/services/api.dart';
import 'package:music_player/core/services/home_service.dart';
import 'package:music_player/core/viewmodels/home_model.dart';
import 'package:music_player/ui/views/home_view.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory(() => HomeView());
  locator.registerFactory(() => HomeModel());
  locator.registerFactory(() => HomeServices());
  locator.registerLazySingleton(() => Api());
}
