import 'package:flutter/material.dart';
import 'package:music_player/locator.dart';
import 'package:music_player/theme.dart';
import 'ui/router.dart';
import 'ui/views/home_view.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Music Player',
      theme: themeData(Colors.blue),
      onGenerateRoute: Router.generateRoute,
      initialRoute: HomeView.pathName,
    );
  }
}
