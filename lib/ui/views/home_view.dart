import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:music_player/core/viewmodels/home_model.dart';
import 'package:music_player/ui/views/base_view.dart';
import 'package:music_player/ui/widgets/audio_player.dart';

class HomeView extends StatefulWidget {
  static const pathName = 'home_view';
  static const title = 'Home';

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  TextEditingController searchKeyController = TextEditingController();
  HomeModel model;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BaseView<HomeModel>(
          onModelReady: (model) async {
            this.model = model;
            await model.getSongsByTerm(searchKeyController.text);
          },
          builder: (context, model, child) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  TextField(
                    controller: searchKeyController,
                    decoration: new InputDecoration(hintText: "Search"),
                    onChanged: (newText) {
                      model.getSongsByTerm(newText);
                    },
                  ),
                  getView(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget getView() {
    if (model.apiResponse != null && model.apiResponse.error != null) {
      return Expanded(
        child: Container(
          child: Center(
              child: Text(
            "Oops!\n\nLooks like something is wrong, Please try again later",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline5,
          )),
        ),
      );
    } else if (searchKeyController.text.isEmpty) {
      return Expanded(
        child: Container(
          child: Center(
              child: Text(
            'Enter search query for results',
            style: Theme.of(context).textTheme.headline5,
          )),
        ),
      );
    } else {
      if (model.apiResponse != null && model.apiResponse.resultCount == 0) {
        return Expanded(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.data_usage,
                  size: 72,
                ),
                SizedBox(height: 16),
                Text(
                  "No Data Found",
                  style: Theme.of(context).textTheme.subtitle1,
                )
              ],
            ),
          ),
        );
      } else {
        return Expanded(
          child: ListView.builder(
            itemCount: model?.apiResponse?.resultCount ?? 0,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  showBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      List<String> urls = model.apiResponse.results.map((e) => e.previewUrl).toList();
                      return Container(
                        height: 200,
                        color: Colors.transparent,
                        child: Center(
                            child: PlayerView(
                          currentIndex: index,
                          urls: urls,
                        )),
                      );
                    },
                  );
                  // model.playAudio(model.apiResponse.results[index].previewUrl);
                },
                child: Card(
                  color: Colors.white,
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(children: [
                      Flexible(
                        flex: 2,
                        child: Image(
                          image: NetworkImage(model.apiResponse.results[index].artworkUrl100),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        flex: 6,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              '${model.apiResponse.results[index].trackName}',
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              '${model.apiResponse.results[index].artistName}',
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              '${model.apiResponse.results[index].collectionName}',
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.subtitle2,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        flex: 2,
                        child: model.currentTrackUrl == model.apiResponse.results[index].previewUrl
                            ? const Icon(Icons.music_note)
                            : Container(),
                      )
                    ]),
                  ),
                ),
              );
            },
          ),
        );
      }
    }
  }
}
