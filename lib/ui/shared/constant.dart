import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final kBackgroundColor = Colors.grey[200];
final kDateFormat = DateFormat("yyyy-MM-ddTHH:mm:ss");
final kAllowedVideoLength = 5 * 60 * 1000;
