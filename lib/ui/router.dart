import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:music_player/locator.dart';
import 'package:music_player/ui/views/home_view.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeView.pathName:
        return MaterialPageRoute(builder: (_) => locator<HomeView>(), settings: RouteSettings(name: HomeView.pathName));

      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}
