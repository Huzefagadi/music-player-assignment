import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class PlayerView extends StatefulWidget {
  int currentIndex;
  List<String> urls;

  PlayerView({this.currentIndex, this.urls});

  @override
  _PlayerViewState createState() => _PlayerViewState();
}

class _PlayerViewState extends State<PlayerView> {
  final AudioPlayer audioPlayer = AudioPlayer(playerId: 'newPlayer');

  play() {
    audioPlayer.play(widget.urls[widget.currentIndex], isLocal: false);
  }

  pause() {
    audioPlayer.pause();
  }

  stop() {
    audioPlayer.stop();
  }

  seek(double value) {
    audioPlayer.seek(Duration(seconds: value.toInt()));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    play();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: Icon(Icons.skip_previous, size: 48),
                onPressed: () {
                  stop();
                  if (widget.currentIndex < 0) {
                    widget.currentIndex = widget.urls.length - 1;
                  } else {
                    widget.currentIndex--;
                  }
                  setState(() {});
                  play();
                },
              ),
              StreamBuilder<AudioPlayerState>(
                stream: audioPlayer.onPlayerStateChanged,
                builder: (_, snapshot) {
                  return AnimatedSwitcher(
                    duration: Duration(milliseconds: 200),
                    child: snapshot.data == AudioPlayerState.PLAYING
                        ? IconButton(
                            icon: Icon(Icons.pause, size: 48),
                            onPressed: pause,
                          )
                        : IconButton(
                            icon: Icon(Icons.play_arrow, size: 48),
                            onPressed: play,
                          ),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.skip_next, size: 48),
                onPressed: () {
                  stop();
                  if (widget.currentIndex == widget.urls.length - 1) {
                    widget.currentIndex = 0;
                  } else {
                    widget.currentIndex++;
                  }
                  setState(() {});
                  play();
                },
              ),
            ],
          ),
          SizedBox(height: 16),
          StreamBuilder<Duration>(
            stream: audioPlayer.onDurationChanged,
            builder: (_, duration) => StreamBuilder<Duration>(
              stream: audioPlayer.onAudioPositionChanged,
              builder: (_, position) => Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Slider(
                      value: position.data?.inSeconds?.toDouble() ?? 0,
                      max: duration.data?.inSeconds?.toDouble() ?? 0,
                      onChangeStart: (value) {
                        pause();
                      },
                      onChanged: seek,
                      onChangeEnd: (value) {
                        play();
                      },
                      activeColor: Colors.black,
                      inactiveColor: Colors.black26,
                    ),
                  ),
                  (duration.hasData)
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 32.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('${position.data.toHHmm()}'),
                              Text(
                                '${(duration.data - position.data).toHHmm()}',
                              ),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}

extension DurationExtension on Duration {
  String toHHmm() {
    return "${this?.inMinutes?.remainder(60)?.toString()?.padLeft(2, "0") ?? "--"}:${this?.inSeconds?.remainder(60)?.toString()?.padLeft(2, "0") ?? "--"}";
  }
}
