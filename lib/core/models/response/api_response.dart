class ApiResponse {
  ApiResponse({this.resultCount, this.results});
  ApiResponse.withError(String errorValue) {
    error = errorValue;
  }

  ApiResponse.fromJson(Map<String, dynamic> json) {
    resultCount = json['resultCount'] as int;
    if (json['results'] != null) {
      results = List<Results>();
      json['results'].forEach((v) {
        results.add(Results.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['resultCount'] = this.resultCount;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }

  int resultCount;
  List<Results> results;
  String error;
}

class Results {
  String wrapperType;
  String kind;
  int artistId;
  int collectionId;
  int trackId;
  String artistName;
  String collectionName;
  String trackName;
  String collectionCensoredName;
  String trackCensoredName;
  int collectionArtistId;
  String collectionArtistName;
  String collectionArtistViewUrl;
  String artistViewUrl;
  String collectionViewUrl;
  String trackViewUrl;
  String previewUrl;
  String artworkUrl30;
  String artworkUrl60;
  String artworkUrl100;
  double collectionPrice;
  double trackPrice;
  String releaseDate;
  String collectionExplicitness;
  String trackExplicitness;
  int discCount;
  int discNumber;
  int trackCount;
  int trackNumber;
  int trackTimeMillis;
  String country;
  String currency;
  String primaryGenreName;
  String contentAdvisoryRating;
  bool isStreamable;

  Results(
      {this.wrapperType,
      this.kind,
      this.artistId,
      this.collectionId,
      this.trackId,
      this.artistName,
      this.collectionName,
      this.trackName,
      this.collectionCensoredName,
      this.trackCensoredName,
      this.collectionArtistId,
      this.collectionArtistName,
      this.collectionArtistViewUrl,
      this.artistViewUrl,
      this.collectionViewUrl,
      this.trackViewUrl,
      this.previewUrl,
      this.artworkUrl30,
      this.artworkUrl60,
      this.artworkUrl100,
      this.collectionPrice,
      this.trackPrice,
      this.releaseDate,
      this.collectionExplicitness,
      this.trackExplicitness,
      this.discCount,
      this.discNumber,
      this.trackCount,
      this.trackNumber,
      this.trackTimeMillis,
      this.country,
      this.currency,
      this.primaryGenreName,
      this.contentAdvisoryRating,
      this.isStreamable});

  Results.fromJson(Map<String, dynamic> json) {
    wrapperType = json['wrapperType'] as String;
    kind = json['kind'] as String;
    artistId = json['artistId'] as int;
    collectionId = json['collectionId'] as int;
    trackId = json['trackId'] as int;
    artistName = json['artistName'] as String;
    collectionName = json['collectionName'] as String;
    trackName = json['trackName'] as String;
    collectionCensoredName = json['collectionCensoredName'] as String;
    trackCensoredName = json['trackCensoredName'] as String;
    collectionArtistId = json['collectionArtistId'] as int;
    collectionArtistName = json['collectionArtistName'] as String;
    collectionArtistViewUrl = json['collectionArtistViewUrl'] as String;
    artistViewUrl = json['artistViewUrl'] as String;
    collectionViewUrl = json['collectionViewUrl'] as String;
    trackViewUrl = json['trackViewUrl'] as String;
    previewUrl = json['previewUrl'] as String;
    artworkUrl30 = json['artworkUrl30'] as String;
    artworkUrl60 = json['artworkUrl60'] as String;
    artworkUrl100 = json['artworkUrl100'] as String;
    collectionPrice = json['collectionPrice'] as double;
    trackPrice = json['trackPrice'] as double;
    releaseDate = json['releaseDate'] as String;
    collectionExplicitness = json['collectionExplicitness'] as String;
    trackExplicitness = json['trackExplicitness'] as String;
    discCount = json['discCount'] as int;
    discNumber = json['discNumber'] as int;
    trackCount = json['trackCount'] as int;
    trackNumber = json['trackNumber'] as int;
    trackTimeMillis = json['trackTimeMillis'] as int;
    country = json['country'] as String;
    currency = json['currency'] as String;
    primaryGenreName = json['primaryGenreName'] as String;
    contentAdvisoryRating = json['contentAdvisoryRating'] as String;
    isStreamable = json['isStreamable'] as bool;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['wrapperType'] = this.wrapperType;
    data['kind'] = this.kind;
    data['artistId'] = this.artistId;
    data['collectionId'] = this.collectionId;
    data['trackId'] = this.trackId;
    data['artistName'] = this.artistName;
    data['collectionName'] = this.collectionName;
    data['trackName'] = this.trackName;
    data['collectionCensoredName'] = this.collectionCensoredName;
    data['trackCensoredName'] = this.trackCensoredName;
    data['collectionArtistId'] = this.collectionArtistId;
    data['collectionArtistName'] = this.collectionArtistName;
    data['collectionArtistViewUrl'] = this.collectionArtistViewUrl;
    data['artistViewUrl'] = this.artistViewUrl;
    data['collectionViewUrl'] = this.collectionViewUrl;
    data['trackViewUrl'] = this.trackViewUrl;
    data['previewUrl'] = this.previewUrl;
    data['artworkUrl30'] = this.artworkUrl30;
    data['artworkUrl60'] = this.artworkUrl60;
    data['artworkUrl100'] = this.artworkUrl100;
    data['collectionPrice'] = this.collectionPrice;
    data['trackPrice'] = this.trackPrice;
    data['releaseDate'] = this.releaseDate;
    data['collectionExplicitness'] = this.collectionExplicitness;
    data['trackExplicitness'] = this.trackExplicitness;
    data['discCount'] = this.discCount;
    data['discNumber'] = this.discNumber;
    data['trackCount'] = this.trackCount;
    data['trackNumber'] = this.trackNumber;
    data['trackTimeMillis'] = this.trackTimeMillis;
    data['country'] = this.country;
    data['currency'] = this.currency;
    data['primaryGenreName'] = this.primaryGenreName;
    data['contentAdvisoryRating'] = this.contentAdvisoryRating;
    data['isStreamable'] = this.isStreamable;
    return data;
  }
}

class UsersResource {
  String createdDateTime;
  String updatedDateTime;
  String firstName;
  String lastName;
  String email;
  String userLastLogin;
  String accountStatus;

  UsersResource(
      {this.createdDateTime,
      this.updatedDateTime,
      this.firstName,
      this.lastName,
      this.email,
      this.userLastLogin,
      this.accountStatus});

  UsersResource.fromJson(Map<String, dynamic> json) {
    createdDateTime = json['createdDateTime'] as String;
    updatedDateTime = json['updatedDateTime'] as String;
    firstName = json['firstName'] as String;
    lastName = json['lastName'] as String;
    email = json['email'] as String;
    userLastLogin = json['userLastLogin'] as String;
    accountStatus = json['accountStatus'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['createdDateTime'] = this.createdDateTime;
    data['updatedDateTime'] = this.updatedDateTime;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['userLastLogin'] = this.userLastLogin;
    data['accountStatus'] = this.accountStatus;
    return data;
  }
}

class Page {
  int size;
  int totalElements;
  int totalPages;
  int number;

  Page({this.size, this.totalElements, this.totalPages, this.number});

  Page.fromJson(Map<String, dynamic> json) {
    size = json['size'] as int;
    totalElements = json['totalElements'] as int;
    totalPages = json['totalPages'] as int;
    number = json['number'] as int;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['size'] = this.size;
    data['totalElements'] = this.totalElements;
    data['totalPages'] = this.totalPages;
    data['number'] = this.number;
    return data;
  }
}
