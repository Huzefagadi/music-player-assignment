import 'package:audioplayers/audioplayers.dart';
import 'package:music_player/core/models/response/api_response.dart';
import 'package:music_player/core/services/home_service.dart';
import 'package:music_player/core/viewmodels/base_model.dart';
import 'package:music_player/locator.dart';

class HomeModel extends BaseModel {
  final HomeServices _homeService = locator<HomeServices>();
  ApiResponse apiResponse;
  String currentTrackUrl = "--";

  Future<void> getSongsByTerm(String term) async {
    setState(ViewState.Busy);
    apiResponse = await _homeService.getSongsByTerm(term);
    setState(ViewState.Idle);
  }
}
