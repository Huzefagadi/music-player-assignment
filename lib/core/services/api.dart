import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:music_player/core/models/response/api_response.dart';

class Api {
  static BaseOptions _options = new BaseOptions(
    baseUrl: "https://itunes.apple.com/search?media=music&",
    headers: {"Accept": "application/json"},
    connectTimeout: 30000,
    followRedirects: false,
    validateStatus: (status) {
      return status < 500;
    },
    receiveTimeout: 30000,
  );
  Dio _dio = new Dio(_options);

  Api() {
    _dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      return options; //continue
    }, onResponse: (Response response) async {
      print("=============RESPONSE==============");
      print(response);
      print("=============RESPONSE==============");
      return response; // continue
    }, onError: (DioError e) async {
      print(e);
      return e; //continue
    }));
  }

  Future<ApiResponse> getSongsByTerm(String term) async {
    try {
      Response response = await _dio.get("term=" + term);
      print("-----------------------------------------------------------------");
      print(response.data);
      print("-----------------------------------------------------------------");
      print(response.statusCode);
      if (response.statusCode == 200) {
        return ApiResponse.fromJson(jsonDecode(response.data as String) as Map<String, dynamic>);
      } else if (response.statusCode == 400) {
        return ApiResponse.withError("Error 400");
      } else {
        return ApiResponse.withError("Error " + response.statusCode.toString());
      }
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ApiResponse.withError(error.toString());
    }
  }
}
