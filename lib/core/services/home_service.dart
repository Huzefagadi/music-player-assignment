import 'package:music_player/core/models/response/api_response.dart';
import 'package:music_player/core/services/api.dart';
import 'package:music_player/locator.dart';

class HomeServices {
  Api api = locator<Api>();

  Future<ApiResponse> getSongsByTerm(String term) {
    return api.getSongsByTerm(term);
  }
}
